import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules'
Vue.use(Vuex)

const state = {
  // 登录用户信息
  userInfo: null
}

const mutations = {
  // 设置已登录的用户信息
  setUserInfo: (state, data) => {
    state.userInfo = data
  }
}
const actions = {}
const getters = {}
export default new Vuex.Store({
  modules,
  state,
  mutations,
  actions,
  getters
})
