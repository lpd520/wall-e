import config from '@/config'
const request = function({url, data, method, resolve, reject}) {
	uni.request({
		url: `${config.BASE_URI}/${url}`,
		method: method,
		header: {
			'walle-user-token': uni.getStorageSync('walle-user-token')
		},
		data,
		success: (res) => {
			// 业务失败
			if (res.data.code !== 200) {
				reject(res.data)
				return
			}
			resolve(res.data.data)
		},
		fail: (err) => {
			reject(err)
		}
	})
}
export default {
	get(url, params) {
		let paramstr = ''
		if (params != null && JSON.stringify(params) !== '{}') {
			for(const key in params) {
				paramstr += '&' + key + '=' + params[key]
			}
			paramstr = '?' + paramstr.substring(1)
		}
		return new Promise((resolve, reject) => {
			request({
				url: url + paramstr,
				method: 'GET',
				resolve,
				reject
			})
		})
	},
	post(url, data) {
		return new Promise((resolve, reject) => {
			request({
				url,
				method: 'POST',
				data,
				resolve,
				reject
			})
		})
	}
}
