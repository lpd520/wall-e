export default {
  install (Vue) {
    // 自定义全局过滤器示例
    Vue.filter('myFilter', value => {
			if (value) {
				return '是'
			}
			return '否'
    })
  }
}
