# uniapp基础工程（研发中）

一套功能完备的uniapp基础工程，内置扫码、定位等常用功能，同时还有一些常用的界面，如个人中心、地址管理、商品列表等，

# SpringCloud Alibaba 基础工程（研发中）

一套基于SpringCloud Alibaba研发的基础工程，适用于开发互联网项目后端，使用nacos作为配置中心和注册中心，使用seata作为分布式事务处理方案。
