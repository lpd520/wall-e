package com.walle.api.common;

import com.walle.api.BaseController;
import com.walle.core.model.ApiResponse;
import com.walle.core.utils.OSS;
import com.walle.core.utils.Utils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Wall-E.Caesar Liu
 * @date 2021/11/23 22:14
 */
@Api(tags = "上传文件")
@RestController
@RequestMapping("/upload")
public class FileUploadController extends BaseController {

    @ApiOperation("上传图片")
    @PostMapping("/image")
    public ApiResponse<OSS.UploadResult> uploadImage(MultipartFile file) {
        return ApiResponse.success(Utils.OSS.setMaxSize(5).uploadImage(file));
    }

    @ApiOperation("上传文件")
    @PostMapping("/attach")
    public ApiResponse<OSS.UploadResult> uploadFile(MultipartFile file) {
        return ApiResponse.success(Utils.OSS.upload(file));
    }
}
