package com.walle.core.utils;

import org.springframework.util.DigestUtils;

/**
 * 安全处理工具类
 * @author Wall-E.Caesar Liu
 * @date 2021/11/23 22:14
 */
public class Secure {

    /**
     * 加密密码
     * @param password 密码
     * @param salt 密码盐
     *
     * @return String
     */
    public String encryptPassword(String password, String salt) {
        return DigestUtils.md5DigestAsHex((password + salt).getBytes());
    }
}
